/*
    Copyright (C) 2013  David Edmundson <davidedmundson@kde.org>
    Copyright (C) 2018  Igor Poboiko <igor.poboiko@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "akonadidatasource.h"
#include "globalcontactmodel.h"

#include <AkonadiCore/Item>
#include <AkonadiCore/Collection>
#include <AkonadiCore/EntityTreeModel>

#include <KPluginFactory>
#include <KPluginLoader>

#include <KContacts/Addressee>
#include <KContacts/ContactGroup>
#include <KContacts/PhoneNumber>
#include <KContacts/Picture>

#include <QPixmap>
#include <QAbstractItemModel>
#include <QAbstractProxyModel>
#include <QModelIndex>

#define AKONADI_SOURCE_PLUGIN_ID QStringLiteral("akonadi")

using namespace Akonadi;
using namespace KPeople;

const QString AkonadiContact::AddresseeProperty = QStringLiteral("addressee");

class AkonadiAllContacts : public KPeople::AllContactsMonitor
{
    Q_OBJECT
public:
    AkonadiAllContacts(QAbstractItemModel *model);
    ~AkonadiAllContacts() override;
    QMap<QString, AbstractContact::Ptr> contacts() override;
private Q_SLOTS:
    void slotRowsInserted(const QModelIndex &parent, int start, int end);
    void slotRowsRemoved(const QModelIndex &parent, int start, int end);
    void slotDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
private:
    QAbstractItemModel *m_model;
    QMap<QString, AbstractContact::Ptr> m_contacts;
    QMap<QString, int> m_contactsRefCount;
};

QString AkonadiContact::toKPeopleUrl(const QUrl &url)
{
    return QStringLiteral("%1://%2").arg(AKONADI_SOURCE_PLUGIN_ID, url.toString(QUrl::RemoveScheme));
}

AkonadiContact::AkonadiContact(const Akonadi::Item& item)
    : m_item(item)
{
    Q_ASSERT(m_item.hasPayload<KContacts::Addressee>());
}

void AkonadiContact::setItem(const Akonadi::Item& item) {
    m_item = item;
}

QVariant AkonadiContact::customProperty(const QString &key) const
{
    KContacts::Addressee contact = m_item.payload<KContacts::Addressee>();
    if (key == AbstractContact::NameProperty)
        return contact.realName();
    else if (key == AbstractContact::GroupsProperty) {
        const Akonadi::Tag::List tags = m_item.tags();
        QVariantList result;
        for (const Akonadi::Tag& t : tags) {
            result << t.name();
        }
        return result;
    } else if (key == AbstractContact::EmailProperty)
        return contact.preferredEmail();
    else if (key == AbstractContact::AllEmailsProperty)
        return contact.emails();
    else if (key == AbstractContact::PhoneNumberProperty) {
        const KContacts::PhoneNumber::List phoneNumbers = contact.phoneNumbers();
        for (const KContacts::PhoneNumber& p : phoneNumbers) {
            if (p.type() & KContacts::PhoneNumber::Pref) {
                return p.number();
            }
        }
        return QVariant();
    } else if (key == AbstractContact::AllPhoneNumbersProperty) {
        const KContacts::PhoneNumber::List phoneNumbers = contact.phoneNumbers();
        QVariantList result;
        for (const KContacts::PhoneNumber& p : phoneNumbers) {
            result << p.number();
        }
        return result;
    } else if (key == AbstractContact::PictureProperty) {
        KContacts::Picture photo = contact.photo();
        if (photo.isEmpty())
            return QVariant();
        else if (photo.isIntern())
            return QPixmap::fromImage(photo.data());
        else
            return photo.url();
    } else if (key == AkonadiContact::AddresseeProperty) {
        return QVariant::fromValue(contact);
    }
    return QVariant();
}

AkonadiContactGroup::AkonadiContactGroup(const Item &item, QAbstractItemModel *model)
    : m_item(item)
    , m_model(model)
{
    Q_ASSERT(m_item.hasPayload<KContacts::ContactGroup>());
    resolveContactGroup();
}

void AkonadiContactGroup::setItem(const Item &item)
{
    m_item = item;
    resolveContactGroup();
}

void AkonadiContactGroup::resolveContactGroup()
{
    m_contacts.clear();
    KContacts::ContactGroup group = m_item.payload<KContacts::ContactGroup>();
    // Populating list with Data's
    for (int i = 0; i < group.dataCount(); i++) {
        const KContacts::ContactGroup::Data data = group.data(i);
        KContacts::Addressee contact;
        contact.setNameFromString(data.name());
        contact.insertEmail(data.email());
        m_contacts << contact;
    }
    // Fetching ETM via bunch of proxies, code used from Akonadi::modelIndexesForSource
    const QAbstractProxyModel *proxy = qobject_cast<const QAbstractProxyModel*>(m_model);
    const QAbstractItemModel *_model = m_model;
    while (proxy) {
        _model = proxy->sourceModel();
        proxy = qobject_cast<const QAbstractProxyModel*>(_model);
    }
    const Akonadi::EntityTreeModel* etm = qobject_cast<const EntityTreeModel*>(_model);
    // Populating lists with ContactReferences
    for (int i = 0; i < group.contactReferenceCount(); i++) {
        const KContacts::ContactGroup::ContactReference reference = group.contactReference(i);
        KContacts::Addressee contact;
        // Fetching an item from ETM
        Akonadi::Item item;
        if (!reference.gid().isEmpty()) {
            item.setGid(reference.gid());
        } else {
            item.setId(reference.uid().toLongLong());
        }
        QModelIndexList idx = Akonadi::EntityTreeModel::modelIndexesForItem(etm, item);
        if (!idx.isEmpty()) {
            item = idx.first().data(Akonadi::EntityTreeModel::ItemRole).value<Akonadi::Item>();
            if (item.isValid() && item.hasPayload<KContacts::Addressee>()) {
                contact = item.payload<KContacts::Addressee>();
            }
            if (!reference.preferredEmail().isEmpty()) {
                contact.insertEmail(reference.preferredEmail(), true);
            }
        }

        m_contacts << contact;
    }
}

QVariant AkonadiContactGroup::customProperty(const QString &key) const {
    KContacts::ContactGroup group = m_item.payload<KContacts::ContactGroup>();
    if (key == AbstractContact::NameProperty) {
        return group.name();
    } else if (key == AbstractContact::AllEmailsProperty) {
        QVariantList result;
        for (const KContacts::Addressee& addressee : qAsConst(m_contacts)) {
            result << addressee.preferredEmail();
        }
        return result;
    }
    // TODO: fancy default icon for a group?
    return QVariant();
}

AkonadiAllContacts::AkonadiAllContacts(QAbstractItemModel *model)
    : m_model(model)
{
    connect(m_model, &QAbstractItemModel::rowsInserted,
            this, &AkonadiAllContacts::slotRowsInserted);
    connect(m_model, &QAbstractItemModel::rowsAboutToBeRemoved,
            this, &AkonadiAllContacts::slotRowsRemoved);
    connect(m_model, &QAbstractItemModel::dataChanged,
            this, &AkonadiAllContacts::slotDataChanged);
    // Since we're dealing with ETM, it's safe to emit initialFetchComplete right away
    emitInitialFetchComplete(true);
}

AkonadiAllContacts::~AkonadiAllContacts()
{
}

QMap<QString, AbstractContact::Ptr> AkonadiAllContacts::contacts()
{
    return m_contacts;
}


void AkonadiAllContacts::slotRowsInserted(const QModelIndex &parent, int start, int end)
{
    for (int row = start; row <= end; row++) {
        QModelIndex idx = m_model->index(row, 0, parent);
        const Akonadi::Item item = idx.data(Akonadi::EntityTreeModel::ItemRole).value<Akonadi::Item>();
        if (item.hasPayload<KContacts::Addressee>() || item.hasPayload<KContacts::ContactGroup>()) {
            const QString id = AkonadiContact::toKPeopleUrl(item.url());
            // Some virtual collections might return duplicated contacts
            // and we have to refcount them to be sure when exactly does the contact gets deleted
            if (m_contacts.contains(id)) {
                m_contactsRefCount[id] += 1;
                continue;
            }
            m_contactsRefCount[id] = 1;
            AbstractContact* new_contact;
            if (item.hasPayload<KContacts::Addressee>()) {
                new_contact = new AkonadiContact(item);
            } else {
                new_contact = new AkonadiContactGroup(item, m_model);
            }
            AbstractContact::Ptr contact(new_contact);
            m_contacts[id] = contact;
            emit contactAdded(id, contact);
        }
    }
}

void AkonadiAllContacts::slotRowsRemoved(const QModelIndex &parent, int start, int end)
{
    for (int row = start; row <= end; row++) {
        QModelIndex idx = m_model->index(row, 0, parent);
        const Akonadi::Item item = idx.data(Akonadi::EntityTreeModel::ItemRole).value<Akonadi::Item>();
        if (item.hasPayload<KContacts::Addressee>() || item.hasPayload<KContacts::ContactGroup>()) {
            const QString id = AkonadiContact::toKPeopleUrl(item.url());
            m_contactsRefCount[id] -= 1;
            if (m_contactsRefCount[id] == 0) {
                m_contacts.remove(id);
                m_contactsRefCount.remove(id);
                emit contactRemoved(id);
            }
        }
    }
}

void AkonadiAllContacts::slotDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    for (int row = topLeft.row(); row <= bottomRight.row(); row++) {
        QModelIndex idx = m_model->index(row, 0, topLeft.parent());
        const Akonadi::Item item = idx.data(Akonadi::EntityTreeModel::ItemRole).value<Akonadi::Item>();
        if (item.hasPayload<KContacts::Addressee>() || item.hasPayload<KContacts::ContactGroup>()) {
            const QString id = AkonadiContact::toKPeopleUrl(item.url());
            AbstractContact::Ptr contact = m_contacts[id];
            if (item.hasPayload<KContacts::Addressee>())  {
                static_cast<AkonadiContact*>(contact.data())->setItem(item);
            } else {
                static_cast<AkonadiContactGroup*>(contact.data())->setItem(item);
            }
            emit contactChanged(id, contact);
        }
    }
}

AkonadiDataSource::AkonadiDataSource(QObject *parent, const QVariantList &args)
    : BasePersonsDataSource(parent, args)
{
    if (args.count() > 0) {
        m_model = args.first().value<QAbstractItemModel*>();
    }
    if (!m_model) {
        m_model = GlobalContactModel::instance()->model();
    }
}

AkonadiDataSource::~AkonadiDataSource()
{

}

QString AkonadiDataSource::sourcePluginId() const
{
    return AKONADI_SOURCE_PLUGIN_ID;
}

KPeople::AllContactsMonitor *AkonadiDataSource::createAllContactsMonitor()
{
    return new AkonadiAllContacts(m_model);
}

K_PLUGIN_FACTORY(AkonadiDataSourceFactory, registerPlugin<AkonadiDataSource>();)

#include "akonadidatasource.moc"
