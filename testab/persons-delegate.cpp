/*
    Copyright (C) 2011  Martin Klapetek <mklapetek@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "persons-delegate.h"

#include <QUrl>
#include <QStyle>
#include <QPainter>
#include <QApplication>
#include <QStandardPaths>

#include <KIconLoader>
#include <KContacts/Addressee>
#include <KPeople/PersonsModel>

const int SPACING = 8;
const int PHOTO_SIZE = 32;

using namespace KPeople;

PersonsDelegate::PersonsDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{
    m_avatarImagePath = QStandardPaths::locate(QStandardPaths::GenericDataLocation, "kpeople/dummy_avatar.png");
}

PersonsDelegate::~PersonsDelegate()
{

}

void PersonsDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->save();

    painter->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform | QPainter::HighQualityAntialiasing);
    painter->setClipRect(option.rect);

    QStyle *style = QApplication::style();
    style->drawPrimitive(QStyle::PE_PanelItemViewItem, &option, painter);

    QRect contactPhotoRect = option.rect;
    contactPhotoRect.adjust(SPACING, SPACING, SPACING, SPACING);
    contactPhotoRect.setWidth(PHOTO_SIZE);
    contactPhotoRect.setHeight(PHOTO_SIZE);

    QImage avatar = index.data(Qt::DecorationRole).value<QImage>();
    painter->drawImage(contactPhotoRect, avatar);

    painter->drawRect(contactPhotoRect);

    QRect nameRect = option.rect;
    nameRect.adjust(SPACING + PHOTO_SIZE + SPACING, SPACING, 0, 0);

    painter->drawText(nameRect, index.data(Qt::DisplayRole).toString());

    QRect typeRect = option.rect;
    typeRect.setWidth(16);
    typeRect.setHeight(16);

    painter->restore();
}

QSize PersonsDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option)
    Q_UNUSED(index)
    return QSize(128, 48);
}
