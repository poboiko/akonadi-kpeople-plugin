/*
    Copyright (C) 2011  Martin Klapetek <mklapetek@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "main-window.h"
#include "persons-delegate.h"
#include "../globalcontactmodel.h"
#include "../akonadidatasource.h"

#include <QDebug>
#include <QLayout>
#include <QTimer>
#include <QVariantList>
#include <QSortFilterProxyModel>
#include <QMessageBox>

#include <KCategoryDrawer>
#include <KCheckableProxyModel>
#include <KSelectionProxyModel>
#include <KPixmapSequence>
#include <KPixmapSequenceWidget>

#include <KContacts/ContactGroup>

#include <AkonadiCore/Collection>
#include <AkonadiCore/CollectionFetchJob>
#include <AkonadiCore/CollectionFetchScope>
#include <AkonadiCore/EntityMimeTypeFilterModel>

#include <KPeople/PersonsModel>
#include <KPeople/PersonData>
#include <KPeople/Global>
#include <KPeople/Widgets/PersonDetailsView>
#include <KPeople/PersonPluginManager>

using namespace KPeople;
using namespace Akonadi;

static bool isStructuralCollection(const Akonadi::Collection &collection)
{
    const QStringList mimeTypes = {KContacts::Addressee::mimeType(), KContacts::ContactGroup::mimeType()};
    const QStringList collectionMimeTypes = collection.contentMimeTypes();
    for (const QString &mimeType : mimeTypes) {
        if (collectionMimeTypes.contains(mimeType)) {
            return false;
        }
    }
    return true;
}

class StructuralCollectionsNotCheckableProxy : public KCheckableProxyModel
{
public:
    explicit StructuralCollectionsNotCheckableProxy(QObject *parent)
        : KCheckableProxyModel(parent)
    {
    }

    QVariant data(const QModelIndex &index, int role) const override
    {
        if (!index.isValid()) {
            return QVariant();
        }

        if (role == Qt::CheckStateRole) {
            // Don't show the checkbox if the collection can't contain incidences
            const Akonadi::Collection collection
                = index.data(Akonadi::EntityTreeModel::CollectionRole).value<Akonadi::Collection>();
            if (collection.isValid() && isStructuralCollection(collection)) {
                return QVariant();
            }
        }
        return KCheckableProxyModel::data(index, role);
    }
};

MainWindow::MainWindow(QWidget *parent)
    : KMainWindow(parent)
{
    setupUi(this);
    // They're not shown until we select some contact
    m_mergeButton->setVisible(false);
    m_unmergeButton->setVisible(false);
    m_personDetailsView->setVisible(false);

    // We need to populate the combobox with collection for contacts
    m_collectionTree = new Akonadi::EntityMimeTypeFilterModel(this);
    m_collectionTree->setDynamicSortFilter(true);
    m_collectionTree->setSortCaseSensitivity(Qt::CaseInsensitive);
    m_collectionTree->setSourceModel(GlobalContactModel::instance()->model());
    m_collectionTree->addMimeTypeInclusionFilter(Akonadi::Collection::mimeType());
    m_collectionTree->setHeaderGroup(Akonadi::EntityTreeModel::CollectionTreeHeaders);

    // And make them checkable
    m_collectionSelectionModel = new QItemSelectionModel(m_collectionTree);
    StructuralCollectionsNotCheckableProxy *checkableProxyModel = new StructuralCollectionsNotCheckableProxy(this);
    checkableProxyModel->setSelectionModel(m_collectionSelectionModel);
    checkableProxyModel->setSourceModel(m_collectionTree);
    m_collectionView->setModel(checkableProxyModel);

    // We also want to filter ETM based on checked address books
    KSelectionProxyModel *selectionProxyModel = new KSelectionProxyModel(m_collectionSelectionModel);
    selectionProxyModel->setSourceModel(GlobalContactModel::instance()->model());
    selectionProxyModel->setFilterBehavior(KSelectionProxyModel::ChildrenOfExactSelection);

    // Feed this to KPeople
    PersonPluginManager::setAutoloadDataSourcePlugins(false);
    PersonPluginManager::addDataSource("akonadi", new AkonadiDataSource(nullptr, { QVariant::fromValue(selectionProxyModel) }));
    m_personsModel = new PersonsModel(this);

    // Now construct model for filter bar
    m_filterModel = new QSortFilterProxyModel(this);
    m_filterModel->setSourceModel(m_personsModel);
    m_filterModel->setDynamicSortFilter(true);
    m_filterModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    connect(m_filterBar, SIGNAL(filterChanged(QString)),
            m_filterModel, SLOT(setFilterFixedString(QString)));

    // Setting up persons view & delegate...
    m_personsDelegate = new PersonsDelegate(this);
    m_personsView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    m_personsView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_personsView->setModel(m_filterModel);
    m_personsView->setItemDelegate(m_personsDelegate);

    connect(m_personsView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
            this, SLOT(onSelectedContactsChanged(QItemSelection,QItemSelection)));
    connect(m_mergeButton, &QPushButton::clicked,
            this, &MainWindow::onMergeButtonPressed);
    connect(m_unmergeButton, &QPushButton::clicked,
            this, &MainWindow::onUnmergeButtonPressed);
}

MainWindow::~MainWindow()
{
}

void MainWindow::onSelectedContactsChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    Q_UNUSED(selected);
    Q_UNUSED(deselected);

    const QModelIndexList &indexes = m_personsView->selectionModel()->selectedIndexes();

    m_personDetailsView->setVisible(indexes.size() > 0);
    m_mergeButton->setVisible(indexes.size() > 1);
    m_unmergeButton->setVisible(false);

    if (indexes.size() == 0) {
        return;
    }

    QString uri = indexes.first().data(PersonsModel::PersonUriRole).toString();
    PersonData *person = new PersonData(uri, m_personDetailsView);
    m_unmergeButton->setVisible((indexes.size() == 1) && (person->contactUris().count() > 1));
    m_personDetailsView->setPerson(person);
}

void MainWindow::onMergeButtonPressed()
{
    QStringList uris;
    QStringList names;
    const QModelIndexList &selectedIndexes = m_personsView->selectionModel()->selectedIndexes();
    for (const QModelIndex & index : selectedIndexes) {
        uris << index.data(PersonsModel::PersonUriRole).toString();
        names << index.data(Qt::DisplayRole).toString();
    }

    QMessageBox::StandardButton reply = QMessageBox::question(this, i18n("Merge contacts?"),
                                                              i18n("You are about to merge the following contacts:<br/>"
                                                                   "<br/>"
                                                                   "<b>%1</b><br/>"
                                                                   "<br/>"
                                                                   "Proceed?").arg(names.join(",<br/>")),
                                                              QMessageBox::Yes | QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        KPeople::mergeContacts(uris);
    }
}

void MainWindow::onUnmergeButtonPressed()
{
    QModelIndex index = m_personsView->selectionModel()->selectedIndexes().first();
    QString uri = index.data(PersonsModel::PersonUriRole).toString();
    QMessageBox::StandardButton reply = QMessageBox::question(this, i18n("Unmerge contacts?"),
                                                              i18n("You are about to unmerge the contact <b>%1</b>. Proceed?").arg(index.data(Qt::DisplayRole).toString()),
                                                              QMessageBox::Yes | QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        KPeople::unmergeContact(uri);
    }
}
