/*
    Copyright (C) 2011  Martin Klapetek <mklapetek@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QApplication>
#include <KAboutData>
#include "main-window.h"

int main(int argc, char **argv) {
    QApplication app(argc, argv);

    KAboutData aboutData("AddressBook", i18n("PIMO:AddressBook"), "0.1",
                         i18n("PIMO:AddressBook"), KAboutLicense::GPL_V2,
                         i18n("(C) 2012, 2013, Martin Klapetek"));

    aboutData.addAuthor(i18nc("@info:credit", "Martin Klapetek"), i18n("Developer"),
                        QStringLiteral("martin.klapetek@gmail.com"));
    aboutData.setProductName("kdepim/addressbook"); //set the correct name for bug reporting
    KAboutData::setApplicationData(aboutData);

    MainWindow *mainWindow = new MainWindow(0);
    mainWindow->show();

    return app.exec();
}
