/*
    Copyright (C) 2013  David Edmundson <davidedmundson@kde.org>
    Copyright (C) 2018  Igor Poboiko <igor.poboiko@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef AKONADIDATASOURCE_H
#define AKONADIDATASOURCE_H

class QAbstractItemModel;

#include <KPeopleBackend/BasePersonsDataSource>
#include <KContacts/Addressee>
#include <AkonadiCore/Item>

/*
 * This is Akonadi plugin for KPeople based on @c EntityTreeModel
 */
class AkonadiDataSource : public KPeople::BasePersonsDataSource
{
public:
    /*
     * First argument in @p args is @c QAbstractItemModel, which will be used for data
     * If it's not specified, we will create a new @c ContactTreeModel
     */
    AkonadiDataSource(QObject *parent, const QVariantList &args = QVariantList());
    ~AkonadiDataSource() override;
    QString sourcePluginId() const override;
    KPeople::AllContactsMonitor *createAllContactsMonitor() override;
private:
    QAbstractItemModel* m_model;
};

/*
 * Class representing an Akonadi contact, that is @c KContacts::Addressee
 */
class AkonadiContact : public KPeople::AbstractContact
{
public:
    typedef QExplicitlySharedDataPointer<AkonadiContact> Ptr;
    
    static QString toKPeopleUrl(const QUrl &url);
    /** String property representing KContacts::Addressee of this contact */
    static const QString AddresseeProperty;
    
    AkonadiContact(const Akonadi::Item& item);
    virtual QVariant customProperty(const QString &key) const Q_DECL_OVERRIDE;
    void setItem(const Akonadi::Item& item);
private:
    // We have to be sure it always has KContacts::Addressee payload!
    Akonadi::Item m_item;
};

/*
 * Class representing an Akonadi contact group, that is @c KContacts::ContactGroup
 */
class AkonadiContactGroup : public KPeople::AbstractContact
{
public:
    AkonadiContactGroup(const Akonadi::Item& item, QAbstractItemModel* model);
    virtual QVariant customProperty(const QString &key) const Q_DECL_OVERRIDE;
    void setItem(const Akonadi::Item& item);
private:
    void resolveContactGroup();
    Akonadi::Item m_item;
    QAbstractItemModel* m_model;
    KContacts::Addressee::List m_contacts;
};

#endif // AKONADIDATASOURCE_H
